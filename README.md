# Installation guide (work in progress)

## Requirements:
CMAKE 3.13

### On Windows:
VC++ build tools 

### On Linux:
Work in progress


### Install package:

```sh
git clone https://gitlab.com/isantesteban/orhi-python.git
cd orhi-python
git submodule update --init --recursive
pip install .
```

