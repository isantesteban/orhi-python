import orhi
import numpy as np

class ExampleGui(orhi.Application):
    
    def on_launch(self):    
        panel = orhi.gui.Panel("Example panel")
        panel.width = 300
        panel.realtive_height = 1.0
        panel.anchor = orhi.gui.Anchor.TOP_RIGHT
        
        panel.add(orhi.gui.Slider(self.material, "shininess", range=[1, 150], label="B0"))
        panel.add(orhi.gui.Slider(self.values, 0, range=[-2, 2], label="B0"))
        panel.add(orhi.gui.Slider(self.values, 0, range=[-2, 2], label="B0"))
        
        self.add(panel)


if __name__ == "__main__":
    example = ExampleGui()
    example.launch()
