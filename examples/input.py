import orhi

class InputDemo(orhi.Application):
    
    def on_launch(self):
        print("Application launched!!")

        callback = lambda : print("'A' pressed")
        self.event_manager.add_key_callback(orhi.Key.A, callback)

        callback = lambda : self.window.close()
        self.event_manager.add_key_callback(orhi.Key.Escape, callback)

        # eventManager->addKeyCallback(orhi::KeyCode::A, orhi::Action::Release, []() {
        #     LOG_INFO("'A' released");
        # });

        # eventManager->addKeyCallback(orhi::KeyCode::A, orhi::Action::Repeat, []() {
        #     LOG_INFO("'A' repeated");
        # });

        # eventManager->addKeyCallback(orhi::KeyCode::B, orhi::Action::Press, orhi::KeyModifier::Alt, []() {
        #     LOG_INFO("'Alt+B' pressed");
        # });

        # eventManager->addKeyCallback(orhi::KeyCode::B, orhi::Action::Press, orhi::KeyModifier::Shift, []() {
        #     LOG_INFO("'Shift+B' pressed");
        # });

        # eventManager->addKeyCallback(orhi::KeyCode::B, orhi::Action::Press, orhi::KeyModifier::Alt | orhi::KeyModifier::Shift, []() {
        #     LOG_INFO("'Shift+Alt+B' pressed");
        # });

        # eventManager->addKeyCallback(orhi::KeyCode::Escape, [window]() {
        #     LOG_INFO("'Escape' pressed");
        #     window->close();
        # });

        # eventManager->addCursorMoveCallback([](float x, float y) {
        #     LOG_INFO("Cursor position: (" << x << "," << y << ")");
        # });

        # eventManager->addMouseButtonCallback(orhi::MouseButton::Left, []() {
        #     LOG_INFO("Left mouse button pressed!");
        # });

        # eventManager->addMouseButtonCallback(orhi::MouseButton::Right, orhi::Action::Release, []() {
        #     LOG_INFO("Right mouse button released!");
        # });


    # def on_update(self):
    #     print(self.time_manager.current_time)
    #     print(self.time_manager.running_time)


    # def on_fixed_update(self):
    #     print("Fixed update")
        # a = self.time_manager
    

if __name__ == "__main__":
    demo = InputDemo("Input demo")
    demo.launch()

