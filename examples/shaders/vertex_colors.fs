#version 430 core

out vec4 fragColor;

in vec3 fragNormal;
in vec3 fragAlbedo;

void main()
{
    vec3 normal = normalize(fragNormal);

    vec3 keyLightDir =  normalize(vec3(1.5, 1.0, 1.0));
    vec3 fillLightDir = normalize(vec3(-2.0, -1.0, 0.0));
    vec3 backLightDir = normalize(vec3(-1.5, 1.5, -1.5));

    vec3 keyLightColor = 1.0 * vec3(1.0, 1.0, 1.0);
    vec3 fillLightColor = 0.3 * vec3(1.0, 1.0, 1.0);
    vec3 backLightColor = 0.5 * vec3(1.0, 1.0, 1.0);

    vec3 diffuseColor = vec3(0.0);

    float diffuse;
    diffuse = 0.5*dot(normal, keyLightDir) + 0.5; // Wrapped diffuse light
    diffuseColor += diffuse*keyLightColor;

    diffuse = 0.5*dot(normal, fillLightDir) + 0.5; // Wrapped diffuse light
    diffuseColor += diffuse*fillLightColor;

    diffuse = max(dot(normal, backLightDir), 0.0);
    diffuseColor += diffuse*backLightColor;

    fragColor = vec4(diffuseColor * fragAlbedo, 1.0);
    fragColor = clamp(fragColor, 0.0, 1.0);
}
