#version 430 core

layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec3 vertexNormal;
layout (location = 4) in vec3 vertexcolor;

layout (std140, binding = 1) uniform Matrices
{
    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;
    mat4 modelViewMatrix; 
    mat3 normalMatrix;
};

out vec3 fragNormal;
out vec3 fragAlbedo;

void main()
{
    gl_Position = vec4(vertexPosition, 1.0) * modelViewMatrix * projectionMatrix;
    fragNormal = vertexNormal * normalMatrix;
    fragAlbedo = vertexcolor;
}
