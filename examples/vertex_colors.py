import orhi
import numpy as np

class VertexColorExample(orhi.Application):
    
    def on_launch(self):    
        self.scene = orhi.Scene()
 
        # Load material
        with open("shaders/vertex_colors.vs", "r") as f:
            vertex_shader = f.read()

        with open("shaders/vertex_colors.fs", "r") as f:
            fragment_shader = f.read()

        material = orhi.Material(vertex_shader, fragment_shader)

        # Generate mesh
        mesh = orhi.SphereMesh()
        vertex_colors = np.ones_like(mesh.vertices)
        vertex_colors[mesh.vertices[:, 1] > 0] = [0.996, 0.043, 0.266]
        vertex_colors[mesh.vertices[:, 1] < 0] = [0.215, 0.772, 0.921]
        mesh.colors = vertex_colors

        model = orhi.Model(mesh, material)
        model.scale = [0.3, 0.3, 0.3]
        self.scene.add(model)

        # Camera
        camera = orhi.OrthographicCamera()
        camera.position = [0, 0, 1.5]

        # Viewport
        viewport = orhi.gui.Viewport(self.scene, camera)
        self.add(viewport)



if __name__ == "__main__":
    demo = VertexColorExample()
    demo.launch()
