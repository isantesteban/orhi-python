import orhi
import numpy as np

class MaterialExample(orhi.Application):
    
    def on_launch(self):    
        self.scene = orhi.Scene()
        self.scene.scale = [0.5, 0.5, 0.5]
        self.scene.position = [0, 0.1, 0]
        self.values = np.zeros(10)
        mesh = orhi.SphereMesh()
        self.material = orhi.PhongMaterial()
        model = orhi.Model(mesh, self.material)
        self.scene.add(model)

        self.lightNode = orhi.Node()
        self.scene.add(self.lightNode)

        # Lights
        directionalLight = orhi.DirectionalLight()
        directionalLight.position = [0, 1.2, 0]
        self.lightNode.add(directionalLight)

        pointLight = orhi.PointLight()
        pointLight.position = [-1.0, -1.0, 0]
        pointLight.color = orhi.Color(1, 0.3, 0.2)
        self.lightNode.add(pointLight)

        material = orhi.PhongMaterial()
        material.emissive = pointLight.color
        material.diffuse = orhi.Color.BLACK
        material.specular = orhi.Color.BLACK
        mesh = orhi.SphereMesh(0.95, 64)
        sphere = orhi.Model(mesh, material)
        sphere.scale = [0.2, 0.2, 0.2]
        pointLight.add(sphere)

        pointLight = orhi.PointLight()
        pointLight.position = [0.7, -0.7, -0.7]
        pointLight.color = orhi.Color(0.2, 0.3, 1.0)
        pointLight.radius *= 0.1
        self.lightNode.add(pointLight)

        material = orhi.PhongMaterial()
        material.emissive = pointLight.color
        material.diffuse = orhi.Color.BLACK
        material.specular = orhi.Color.BLACK
        sphere = orhi.Model(mesh, material)
        sphere.scale = [0.1, 0.1, 0.1]
        pointLight.add(sphere)

        # Camera
        camera = orhi.OrthographicCamera()
        camera.position = [0, 0, 1.5]

        # User interface
        viewport = orhi.gui.Viewport(self.scene, camera)
        self.add(viewport)

        panel = orhi.gui.Panel()
        panel.width = 300

        panel.add(orhi.gui.Slider(self.material, "shininess", range=[1, 150], label="B0"))
        panel.add(orhi.gui.Slider(self.values, 0, range=[-2, 2], label="B0"))
        panel.add(orhi.gui.Slider(self.values, 0, range=[-2, 2], label="B0"))

        self.add(panel)

    def on_fixed_update(self):
        self.lightNode.euler_angles[1] = 0.001*self.clock.running_time
        self.lightNode.update()



if __name__ == "__main__":
    demo = MaterialExample()
    demo.launch()
