import orhi
import numpy as np

class TestDemo(orhi.Application):
    
    def __init__(self):
        super(TestDemo, self).__init__()

        self.scene = orhi.Scene()
        self.scene.scale = [0.5, 0.5, 0.5]
        self.scene.position = [0, 0.1, 0]

        self.lightNode = orhi.Node()
        self.scene.add(self.lightNode)

        self.values = np.zeros(10)


    def on_launch(self):    
        mesh = orhi.SphereMesh()
        material = orhi.PhongMaterial()
        material.wireframe_visible = True
        # material.wireframe_color = orhi.Color(1.0, 0.0, 0.0, 0.2)
        # material.wireframe_thickness = 2.0
        model = orhi.Model(mesh, material)
        self.scene.add(model)

        # Lights
        directionalLight = orhi.DirectionalLight()
        directionalLight.position = [0, 1.2, 0]
        self.lightNode.add(directionalLight)

        pointLight = orhi.PointLight()
        pointLight.position = [-1.0, -1.0, 0]
        pointLight.color = orhi.Color(1, 0.3, 0.2)
        self.lightNode.add(pointLight)

        material = orhi.PhongMaterial()
        material.emissive = pointLight.color
        material.diffuse = orhi.Color.BLACK
        material.specular = orhi.Color.BLACK
        mesh = orhi.SphereMesh(0.95, 64)
        sphere = orhi.Model(mesh, material)
        sphere.scale = [0.2, 0.2, 0.2]
        pointLight.add(sphere)

        pointLight = orhi.PointLight()
        pointLight.position = [0.7, -0.7, -0.7]
        pointLight.color = orhi.Color(0.2, 0.3, 1.0)
        pointLight.radius *= 0.1
        self.lightNode.add(pointLight)

        material = orhi.PhongMaterial()
        material.emissive = pointLight.color
        material.diffuse = orhi.Color.BLACK
        material.specular = orhi.Color.BLACK
        sphere = orhi.Model(mesh, material)
        sphere.scale = [0.1, 0.1, 0.1]
        pointLight.add(sphere)

        # Camera
        camera = orhi.OrthographicCamera()
        camera.position = [0, 0, 1.5]

        # User interface
        viewport = orhi.gui.Viewport(self.scene, camera)
        self.add(viewport)

        panel = orhi.gui.Panel()
        panel.anchor = orhi.gui.Anchor.BOTTOM_LEFT
        panel.relative_width = 1.0

        panel.add(orhi.gui.Row([
            orhi.gui.Text("Shape coefficients"),
            orhi.gui.Text("(Disabled, only available for custom subjects)", color=orhi.Color.GRAY)
        ]))
  
        panel.add(orhi.gui.Row([
            orhi.gui.Slider(self.values, 0, range=[-2, 2], label="B0"),
            orhi.gui.Slider(self.values, 1, range=[-2, 2], label="B1"),
            orhi.gui.Slider(self.values, 2, range=[-2, 2], label="B2"),
            orhi.gui.Slider(self.values, 3, range=[-2, 2], label="B3"),
            orhi.gui.Slider(self.values, 4, range=[-2, 2], label="B4")
        ]))
        
        panel.add(orhi.gui.Row([
            orhi.gui.Slider(self.values, 5, range=[-2, 2], label="B5"),
            orhi.gui.Slider(self.values, 6, range=[-2, 2], label="B6"),
            orhi.gui.Slider(self.values, 7, range=[-2, 2], label="B7"),
            orhi.gui.Slider(self.values, 8, range=[-2, 2], label="B8"),
            orhi.gui.Slider(self.values, 9, range=[-2, 2], label="B9")
        ]))

        self.add(panel)

        panel = orhi.gui.Panel()
        panel.anchor = orhi.gui.Anchor.TOP_LEFT

        panel.add([
            orhi.gui.Combo(["A", "B", "C"], label="Sequence", on_change=lambda x: print(x)),
            orhi.gui.Combo(["A", "B", "C"], label="Subject", on_change=lambda x: print(x)),
        ])

        self.add(panel)
        

    # def on_fixed_update(self):
        # self.lightNode.euler_angles[1] = 0.001*self.clock.running_time
        # self.lightNode.update()



if __name__ == "__main__":
    demo = TestDemo()
    demo.launch()
