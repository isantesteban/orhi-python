#include <pybind11/pybind11.h>

namespace py = pybind11;

void initApp(py::module &m);
void initGui(py::module &m);
void initScene(py::module &m);
void initMath(py::module &m);
void initUtils(py::module &m);

PYBIND11_MODULE(orhi, m) {
    m.doc() = "TODO: Module description";

#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif

    initApp(m);
    initGui(m);
    initScene(m);
    initMath(m);
    initUtils(m);
}
