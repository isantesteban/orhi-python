#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/eigen.h>
#include <orhi/core.hh>

namespace py = pybind11;

void initScene(py::module &m)
{
    py::class_<orhi::Node, std::shared_ptr<orhi::Node>>(m, "Node")
        .def(py::init<>())
        .def("add", &orhi::Node::add)
        .def_property("position", &orhi::Node::getPosition, py::overload_cast<orhi::Vector3f>(&orhi::Node::setPosition))
        .def_property("euler_angles", &orhi::Node::getEulerAngles, py::overload_cast<orhi::Vector3f>(&orhi::Node::setEulerAngles))
        .def_property("scale", &orhi::Node::getScale, py::overload_cast<orhi::Vector3f>(&orhi::Node::setScale))
        .def_property_readonly("world_transform", &orhi::Node::getWorldTransform)
        .def_property_readonly("local_transform", &orhi::Node::getLocalTransform);

    py::class_<orhi::Scene, orhi::Node, std::shared_ptr<orhi::Scene>>(m, "Scene")
        .def(py::init<>());

    py::class_<orhi::Model, orhi::Node, std::shared_ptr<orhi::Model>>(m, "Model")
        .def(py::init<std::shared_ptr<orhi::Mesh>, std::shared_ptr<orhi::Material>>())
        .def_property("mesh", &orhi::Model::getMesh, &orhi::Model::setMesh)
        .def_property("material", &orhi::Model::getMaterial, &orhi::Model::setMaterial);


    py::class_<orhi::Camera, orhi::Node, std::shared_ptr<orhi::Camera>>(m, "Camera")
        .def_property("near", &orhi::Camera::getNear, &orhi::Camera::setNear)
        .def_property("far", &orhi::Camera::getFar, &orhi::Camera::setFar)
        .def_property("aspect_ratio", &orhi::Camera::getAspectRatio, &orhi::Camera::setAspectRatio)
        .def_property("controls", &orhi::Camera::getControls, &orhi::Camera::setControls)
        .def_property_readonly("projection_matrix", &orhi::Camera::getProjectionMatrix);

    py::class_<orhi::OrthographicCamera, orhi::Camera, std::shared_ptr<orhi::OrthographicCamera>>(m, "OrthographicCamera")
        .def(py::init<>());

    py::class_<orhi::PerspectiveCamera, orhi::Camera, std::shared_ptr<orhi::PerspectiveCamera>>(m, "PerspectiveCamera")
        .def(py::init<>())
        .def_property("fov", &orhi::PerspectiveCamera::getFov, &orhi::PerspectiveCamera::setFov);


    py::class_<orhi::CameraControls, std::shared_ptr<orhi::CameraControls>>(m, "CameraControls"); 

    py::class_<orhi::OrbitControls, orhi::CameraControls, std::shared_ptr<orhi::OrbitControls>>(m, "OrbitControls")
        .def(py::init<>());


    py::class_<orhi::Material, std::shared_ptr<orhi::Material>>(m, "Material")
        .def(py::init<const std::string &, const std::string &>(), py::arg("vertex_shader"), py::arg("fragment_shader"))
        .def("set", py::overload_cast<const std::string &, float>(&orhi::Material::set, py::const_))
        .def("set", py::overload_cast<const std::string &, const orhi::Vector3f &>(&orhi::Material::set, py::const_))
        .def("set", py::overload_cast<const std::string &, const orhi::Vector4f &>(&orhi::Material::set, py::const_))
        .def("set", py::overload_cast<const std::string &, const orhi::Matrix3f &>(&orhi::Material::set, py::const_))
        .def("set", py::overload_cast<const std::string &, const orhi::Matrix4f &>(&orhi::Material::set, py::const_));
     

    py::class_<orhi::PhongMaterial, orhi::Material, std::shared_ptr<orhi::PhongMaterial>>(m, "PhongMaterial")
        .def(py::init<>())
        .def_property("diffuse", &orhi::PhongMaterial::getDiffuse, &orhi::PhongMaterial::setDiffuse)
        .def_property("specular", &orhi::PhongMaterial::getSpecular, &orhi::PhongMaterial::setSpecular)
        .def_property("emissive", &orhi::PhongMaterial::getEmissive, &orhi::PhongMaterial::setEmissive)
        .def_property("shininess", &orhi::PhongMaterial::getShininess, &orhi::PhongMaterial::setShininess)
        .def_property("wireframe_visible", &orhi::PhongMaterial::isWireframeVisible, &orhi::PhongMaterial::setWireframeVisible)
        .def_property("wireframe_color", &orhi::PhongMaterial::getWireframeColor, &orhi::PhongMaterial::setWireframeColor)
        .def_property("wireframe_thickness", &orhi::PhongMaterial::getWireframeThickness, &orhi::PhongMaterial::setWireframeThickness);


    py::class_<orhi::Mesh, std::shared_ptr<orhi::Mesh>>(m, "Mesh")
        .def(py::init<>())
        .def(py::init<const Eigen::Ref<const orhi::MatrixXf>, const Eigen::Ref<const orhi::MatrixXi>>(),
            py::arg("vertices"), py::arg("faces"))
        .def("compute_normals", &orhi::Mesh::computeNormals)
        .def_property("vertices", &orhi::Mesh::getVertices, &orhi::Mesh::setVertices)
        .def_property("faces", &orhi::Mesh::getFaces, &orhi::Mesh::setFaces)
        .def_property("normals", &orhi::Mesh::getNormals, &orhi::Mesh::setNormals)
        .def_property("uvs", &orhi::Mesh::getTextureCoordinates, &orhi::Mesh::setTextureCoordinates)
        .def_property("colors", &orhi::Mesh::getColors, &orhi::Mesh::setColors);

    py::class_<orhi::SphereMesh, orhi::Mesh, std::shared_ptr<orhi::SphereMesh>>(m, "SphereMesh")
        .def(py::init<float, int>(), py::arg("radius") = 1.0f, py::arg("segments") = 64);

    py::class_<orhi::PlaneMesh, orhi::Mesh, std::shared_ptr<orhi::PlaneMesh>>(m, "PlaneMesh")
        .def(py::init<float, float, int>(), py::arg("width") = 1.0f, py::arg("height") = 1.0f, py::arg("segments") = 1);


    py::class_<orhi::Light, orhi::Node, std::shared_ptr<orhi::Light>>(m, "Light")
        .def(py::init<>())
        .def_property("color", &orhi::Light::getColor, &orhi::Light::setColor)
        .def_property("intensity", &orhi::Light::getIntensity, &orhi::Light::setIntensity);

    py::class_<orhi::PointLight, orhi::Light, std::shared_ptr<orhi::PointLight>>(m, "PointLight")
        .def(py::init<>())    
        .def_property("radius", &orhi::PointLight::getRadius, &orhi::PointLight::setRadius);

    py::class_<orhi::DirectionalLight, orhi::Light, std::shared_ptr<orhi::DirectionalLight>>(m, "DirectionalLight")
        .def(py::init<>());


}