#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <orhi/core.hh>
#include <imgui/imgui.h>

namespace py = pybind11;

namespace orhi::gui
{
class PySlider : public orhi::gui::Widget
{
public:
    void render() override
    {
        float value = m_getValue();

        if (ImGui::SliderFloat((m_label + m_id).c_str(), &value, m_range(0), m_range(1), "%.2f"))
        {
            m_setValue(value);

            if (m_onChange)
            {
                m_onChange(value);
            }
        }
    }

    PySlider(py::list list, int index, Vector2f range, const std::string &label = "", std::function<void(float)> onChange = {})
    {
        m_getValue = [list, index]() -> float { return list[index].cast<float>(); };
        m_setValue = [list, index](float value) { list[index] = value; };
        m_range = range;
        m_label = label;
        m_onChange = onChange;
        updateSize();
    }

    PySlider(py::array array, py::object index, Vector2f range, const std::string &label = "", std::function<void(float)> onChange = {})
    {
        m_getValue = [array, index]() -> float { return array[index].cast<float>(); };
        m_setValue = [array, index](float value) { array[index] = value; };
        m_range = range;
        m_label = label;
        m_onChange = onChange;
        updateSize();
    }

    PySlider(py::dict dict, py::object key, Vector2f range, const std::string &label = "", std::function<void(float)> onChange = {})
    {
        m_getValue = [dict, key]() -> float { return dict[key].cast<float>(); };
        m_setValue = [dict, key](float value) { dict[key] = value; };
        m_range = range;
        m_label = label;
        m_onChange = onChange;
        updateSize();
    }

    PySlider(py::object obj, py::str attr, Vector2f range, const std::string &label = "", std::function<void(float)> onChange = {})
    {
        m_getValue = [obj, attr]() -> float { return obj.attr(attr).cast<float>(); };
        m_setValue = [obj, attr](float value) { obj.attr(attr) = value; };
        m_range = range;
        m_label = label;
        m_onChange = onChange;
        updateSize();
    }

private:
    std::string m_label;

    std::function<float()> m_getValue;
    std::function<void(float)> m_setValue;
    std::function<void(float)> m_onChange;

    Vector2f m_range;

    void updateSize()
    {
        ImGuiStyle &style = ImGui::GetStyle();
        m_size = Style::CalcTextSize(m_label);
        m_size(0) += 1.0f * style.ItemInnerSpacing.x;
        m_size(1) += 2.0f * style.FramePadding.y;
    }
};

} // namespace orhi::gui

void initGui(py::module &m)
{
    auto mgui = m.def_submodule("gui");

    py::class_<orhi::gui::Widget, std::shared_ptr<orhi::gui::Widget>>(mgui, "Widget")
        .def_property("anchor", &orhi::gui::Widget::getAnchor, &orhi::gui::Widget::setAnchor)
        .def_property("position", &orhi::gui::Widget::getPosition, py::overload_cast<orhi::Vector2i>(&orhi::gui::Widget::setPosition))
        .def_property("relative_position", &orhi::gui::Widget::getRelativePosition, py::overload_cast<orhi::Vector2f>(&orhi::gui::Widget::setRelativePosition))
        .def_property("size", &orhi::gui::Widget::getSize, py::overload_cast<orhi::Vector2i>(&orhi::gui::Widget::setSize))
        .def_property("height", &orhi::gui::Widget::getHeight, py::overload_cast<int>(&orhi::gui::Widget::setHeight))
        .def_property("width", &orhi::gui::Widget::getWidth, py::overload_cast<int>(&orhi::gui::Widget::setWidth))
        .def_property("relative_size", &orhi::gui::Widget::getRelativeSize, py::overload_cast<orhi::Vector2f>(&orhi::gui::Widget::setRelativeSize))
        .def_property("relative_height", &orhi::gui::Widget::getRelativeHeight, py::overload_cast<float>(&orhi::gui::Widget::setRelativeHeight))
        .def_property("relative_width", &orhi::gui::Widget::getRelativeWidth, py::overload_cast<float>(&orhi::gui::Widget::setRelativeWidth))
        .def_property("background_color", &orhi::gui::Widget::getBackgroundColor, &orhi::gui::Widget::setBackgroundColor);

    py::class_<orhi::gui::Viewport, orhi::gui::Widget, std::shared_ptr<orhi::gui::Viewport>>(mgui, "Viewport")
        .def(py::init<std::shared_ptr<orhi::Scene>, std::shared_ptr<orhi::Camera>>());

    py::enum_<orhi::gui::Anchor>(mgui, "Anchor")
        .value("TOP_LEFT", orhi::gui::Anchor::TOP_LEFT)
        .value("TOP_CENTER", orhi::gui::Anchor::TOP_CENTER)
        .value("TOP_RIGHT", orhi::gui::Anchor::TOP_RIGHT)
        .value("BOTTOM_LEFT", orhi::gui::Anchor::BOTTOM_LEFT)
        .value("BOTTOM_CENTER", orhi::gui::Anchor::BOTTOM_CENTER)
        .value("BOTTOM_RIGHT", orhi::gui::Anchor::BOTTOM_RIGHT)
        .value("CENTER_LEFT", orhi::gui::Anchor::CENTER_LEFT)
        .value("CENTER_RIGHT", orhi::gui::Anchor::CENTER_RIGHT)
        .value("CENTER", orhi::gui::Anchor::CENTER);

    py::class_<orhi::gui::Style, std::shared_ptr<orhi::gui::Style>>(mgui, "Style")
        .def(py::init<>())
        .def_property("scale", &orhi::gui::Style::getScale, &orhi::gui::Style::setScale)
        .def_property("em", &orhi::gui::Style::getEM, &orhi::gui::Style::setEM);

    py::class_<orhi::gui::Container, orhi::gui::Widget, std::shared_ptr<orhi::gui::Container>>(mgui, "Container")
        .def("add", py::overload_cast<std::shared_ptr<orhi::gui::Widget>>(&orhi::gui::Container::add))
        .def("add", py::overload_cast<const std::vector<std::shared_ptr<orhi::gui::Widget>> &>(&orhi::gui::Container::add));

    py::class_<orhi::gui::Panel, orhi::gui::Container, std::shared_ptr<orhi::gui::Panel>>(mgui, "Panel")
        .def(py::init<>())
        .def(py::init<const std::string &>());

    py::class_<orhi::gui::Row, orhi::gui::Container, std::shared_ptr<orhi::gui::Row>>(mgui, "Row")
        .def(py::init<>())
        .def(py::init<const std::vector<std::shared_ptr<orhi::gui::Widget>> &>());

    py::class_<orhi::gui::Text, orhi::gui::Widget, std::shared_ptr<orhi::gui::Text>>(mgui, "Text")
        .def(py::init<const std::string &>())
        .def(py::init<const std::string &, orhi::Color>(), py::arg("text"), py::arg("color"));

    py::class_<orhi::gui::Combo, orhi::gui::Widget, std::shared_ptr<orhi::gui::Combo>>(mgui, "Combo")
        .def(py::init<const std::vector<std::string> &, const std::string &, std::function<void(std::string)>, int>(),
             py::arg("list"), py::arg("label") = "", py::arg("on_change") = nullptr, py::arg("current_index") = 0);

    py::class_<orhi::gui::Checkbox, orhi::gui::Widget, std::shared_ptr<orhi::gui::Checkbox>>(mgui, "Checkbox")
        .def(py::init<const std::string &, std::function<void(bool)>>(),
             py::arg("label") = "", py::arg("on_change") = nullptr);

    py::class_<orhi::gui::Button, orhi::gui::Widget, std::shared_ptr<orhi::gui::Button>>(mgui, "Button")
        .def(py::init<const std::string &, std::function<void()>>(),
             py::arg("label") = "", py::arg("on_pressed") = nullptr);

    py::class_<orhi::gui::SmallButton, orhi::gui::Widget, std::shared_ptr<orhi::gui::SmallButton>>(mgui, "SmallButton")
        .def(py::init<const std::string &, std::function<void()>>(),
             py::arg("label") = "", py::arg("on_pressed") = nullptr);

    py::class_<orhi::gui::PySlider, orhi::gui::Widget, std::shared_ptr<orhi::gui::PySlider>>(mgui, "Slider")
        .def(py::init<py::list, int, orhi::Vector2f, const std::string &, std::function<void(float)>>(), 
            py::arg("list"), py::arg("index"), py::arg("range"), py::arg("label") = "", py::arg("on_change") = nullptr)
        .def(py::init<py::dict, py::object, orhi::Vector2f, const std::string &, std::function<void(float)>>(),
            py::arg("dict"), py::arg("key"), py::arg("range"), py::arg("label") = "", py::arg("on_change") = nullptr)
        .def(py::init<py::array, py::object, orhi::Vector2f, const std::string &, std::function<void(float)>>(),
            py::arg("array"), py::arg("index"), py::arg("range"), py::arg("label") = "", py::arg("on_change") = nullptr)
        .def(py::init<py::object, py::str, orhi::Vector2f, const std::string &, std::function<void(float)>>(),
            py::arg("object"), py::arg("attribute"), py::arg("range"), py::arg("label") = "", py::arg("on_change") = nullptr);

    py::class_<orhi::gui::Separator, orhi::gui::Widget, std::shared_ptr<orhi::gui::Separator>>(mgui, "Separator")
        .def(py::init<>());   
        
    py::class_<orhi::gui::ColorPicker, orhi::gui::Widget, std::shared_ptr<orhi::gui::ColorPicker>>(mgui, "ColorPicker")
        .def(py::init<orhi::Color *, const std::string &, std::function<void(const orhi::Color &)>>(), 
            py::arg("color"), py::arg("label") = "", py::arg("on_change") = nullptr);          
}