#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/eigen.h>
#include <orhi/core.hh>

namespace py = pybind11;

void initMath(py::module &m)
{
    py::class_<orhi::Color>(m, "Color")
        .def(py::init<>())
        .def(py::init<float, float, float>())
        .def(py::init<float, float, float, float>())
        .def_property("alpha", &orhi::Color::getAlpha, &orhi::Color::setAlpha)
        .def_property("rgb", &orhi::Color::getRGB, py::overload_cast<orhi::Vector3f>(&orhi::Color::setRGB))
        .def_property("rgba", &orhi::Color::getRGBA, py::overload_cast<orhi::Vector4f>(&orhi::Color::setRGBA))
        .def_readonly_static("WHITE", &orhi::Color::WHITE)
        .def_readonly_static("BLACK", &orhi::Color::BLACK)
        .def_readonly_static("GRAY", &orhi::Color::GRAY)
        .def_readonly_static("RED", &orhi::Color::RED)
        .def_readonly_static("BLUE", &orhi::Color::BLUE)
        .def_readonly_static("GREEN", &orhi::Color::GREEN);
}