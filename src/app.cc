#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/eigen.h>
#include <orhi/core.hh>

namespace py = pybind11;

namespace orhi 
{
    class PyApplication : public Application {
    public:
        using Application::Application;

        void onLaunch() override {
            PYBIND11_OVERLOAD_NAME(
                void,         /* Return type */
                Application,  /* Parent class */
                "on_launch",  /* Name of function in Python */
                onLaunch,     /* Name of function in C++ */
            );
        }

        void onUpdate() override {
            PYBIND11_OVERLOAD_NAME(
                void,         /* Return type */
                Application,  /* Parent class */
                "on_update",  /* Name of function in Python */
                onUpdate,     /* Name of function in C++ */
            );
        }

        void onFixedUpdate() override {
            PYBIND11_OVERLOAD_NAME(
                void,               /* Return type */
                Application,        /* Parent class */
                "on_fixed_update",  /* Name of function in Python */
                onFixedUpdate,      /* Name of function in C++ */
            );
        }
    };

    // Expose protected methods
    class ApplicationPublicist : public Application
    {
    public:
        using Application::getWindow;
        using Application::getTimeManager;
        using Application::getEventManager;
        using Application::getStyle;
        using Application::onLaunch;
        using Application::onUpdate;
        using Application::onFixedUpdate;
        using Application::add;
    };
}

void initApp(py::module &m)
{
     py::class_<orhi::Application, orhi::PyApplication, std::shared_ptr<orhi::Application>>(m, "Application")
        .def(py::init<>())
        .def(py::init<const std::string &>())
        .def("launch", &orhi::Application::launch)
        .def("add", &orhi::ApplicationPublicist::add)
        .def_property_readonly("window", &orhi::ApplicationPublicist::getWindow)
        .def_property_readonly("events", &orhi::ApplicationPublicist::getEventManager)
        .def_property_readonly("clock", &orhi::ApplicationPublicist::getTimeManager)
        .def_property_readonly("style", &orhi::ApplicationPublicist::getStyle)
        .def("on_launch", &orhi::ApplicationPublicist::onLaunch)
        .def("on_update", &orhi::ApplicationPublicist::onUpdate)
        .def("on_fixed_update", &orhi::ApplicationPublicist::onFixedUpdate);

    py::class_<orhi::TimeManager, std::shared_ptr<orhi::TimeManager>>(m, "TimeManager")
        .def("restart", &orhi::TimeManager::start)
        .def_property("time_step", &orhi::TimeManager::getTimeStep, &orhi::TimeManager::setTimeStep)
        .def_property("current_time", &orhi::TimeManager::getCurrentTime, &orhi::TimeManager::setCurrentTime);

    py::class_<orhi::EventManager, std::shared_ptr<orhi::EventManager>>(m, "EventManager")
        .def("add_key_callback", py::overload_cast<orhi::Key, std::function<void()>>(&orhi::EventManager::addKeyCallback))
        .def("add_key_callback", py::overload_cast<orhi::Key, orhi::Action, std::function<void()>>(&orhi::EventManager::addKeyCallback))
        .def("add_key_callback", py::overload_cast<orhi::Key, orhi::Action, int, std::function<void()>>(&orhi::EventManager::addKeyCallback))
        .def("add_cursor_move_callback", &orhi::EventManager::addCursorMoveCallback)
        .def("add_mouse_button_callback", py::overload_cast<orhi::MouseButton, std::function<void()>>(&orhi::EventManager::addMouseButtonCallback))
        .def("add_mouse_button_callback", py::overload_cast<orhi::MouseButton, orhi::Action, std::function<void()>>(&orhi::EventManager::addMouseButtonCallback))
        .def("add_mouse_button_callback", py::overload_cast<orhi::MouseButton, orhi::Action, int, std::function<void()>>(&orhi::EventManager::addMouseButtonCallback))
        .def("add_resize_callback", &orhi::EventManager::addResizeCallback);

    py::class_<orhi::Window, std::shared_ptr<orhi::Window>>(m, "Window")
        .def("close", &orhi::Window::close)
        .def_property("size", &orhi::Window::getSize, &orhi::Window::setSize)
        .def_property("width", &orhi::Window::getWidth, &orhi::Window::setWidth)
        .def_property("height", &orhi::Window::getHeight, &orhi::Window::setHeight);

    py::enum_<orhi::Key>(m, "Key")
        .value("A", orhi::Key::A)
        .value("Escape", orhi::Key::ESCAPE);
}