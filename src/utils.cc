#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/eigen.h>
#include <orhi/core.hh>

namespace py = pybind11;

void initUtils(py::module &m)
{
    auto mloaders = m.def_submodule("io");
    mloaders.def("load_mesh", &orhi::io::LoadMesh);
}